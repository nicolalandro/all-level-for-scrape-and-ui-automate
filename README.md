# All level for scrape and UI Automate

The idea of this repo is to show how to make scraping with each method possible (one lib for method). 

## Zero Code Tool

For tool I install in the browser [Webscraper.io](https://webscraper.io/) and I test [this sitemap](src/sitemap.json)

## Html Only

For Html Only I try [BeatutifulSoup](https://beautiful-soup-4.readthedocs.io/en/latest/#) with [this example](src/bsoup_ex.py)

## With JS Interprete

For Web browser integration I try [Selenium](https://selenium-python.readthedocs.io/) with [this example](src/selenium_ex.py)

## UI Automation

For UI Automation I try [Sikulix](https://www.youtube.com/watch?v=VdCOg1bCmGo) with [this example](src/sikuli_ex.sikuli)

## Next steps?
* [Testing Libraries](https://gitlab.com/nicolalandro/fastapitest): select one test lib
* [Test Lib + UI/web automation with Streamlit](https://blog.devgenius.io/testing-streamlit-a1f1fd48ce8f): use the selected test lib with an UI/Web automation tool